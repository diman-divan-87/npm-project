import { ListIdStatusObject} from "src/app/namespace";
import {createReducer, on} from '@ngrx/store';
import * as commonActions from './actions';
import { updateListIdsObject} from "./actions";

export interface ICoreState {
    statusList: {
        'to-read': { [key: string]: boolean };
        'in-progress': { [key: string]: boolean };
        'done': { [key: string]: boolean };
    };
    tag: string;
}

export const initialState: ICoreState = {
    statusList: {
        'to-read': {},
        'in-progress': {},
        'done': {}},
    tag: ''
}

const _commonReducer = createReducer(
    initialState,
    on(
        commonActions.updateListIdsObject, (state: ICoreState, data: { listIdsObject: ListIdStatusObject }) => {
            return {
                ...state,
                statusList: {
                    ...state.statusList,
                    'to-read': data.listIdsObject["to-read"],
                    'in-progress': data.listIdsObject["in-progress"],
                    'done': data.listIdsObject["done"]
                }
            };
        }
    ),
        on(
        commonActions.setFilterTag, (state: ICoreState, data: { tag: string }) => {
            return {
                ...state,
                tag: data.tag
            };
        }
    )
)

export function commonReducers(state: any, action: any) {
    return _commonReducer(state, action);
}
