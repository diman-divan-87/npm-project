import { ICoreState } from './reducers';
import { createSelector } from '@ngrx/store';

export interface AppState {
    core: ICoreState;
}

export const coreSelector = (state: AppState) => state.core;

export const selectListIdObject = createSelector(
    coreSelector,
    (state: ICoreState) => state.statusList,
)

export const selectTag = createSelector(
    coreSelector,
    (state: ICoreState) => state.tag,
)
