import { createAction, props } from '@ngrx/store';
import {ListIdStatusObject} from "../namespace";

export enum CommonActions {
    UpdateListIdsObject = '[CommonActions] update object ids',
    setFilterTag = '[CommonActions] set filter tag',

}

export const updateListIdsObject = createAction(CommonActions.UpdateListIdsObject, props<{listIdsObject: ListIdStatusObject}>());
export const setFilterTag = createAction(CommonActions.setFilterTag, props<{tag: string}>());

