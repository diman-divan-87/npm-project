import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AppService} from "../../app.service";
import {Books, BooksStatusName} from "../../namespace";


@Component({
    selector: 'app-book',
    templateUrl: './book.component.html',
    styleUrls: ['./book.component.less']
})
export class BookComponent {
    bookId: string = '';
    book: Books;
    prevPage;
    beforeStatus ;

    constructor(
        private activatedRoute: ActivatedRoute,
        public appService: AppService) {
        this.bookId = this.activatedRoute.snapshot.params.id;
        this.updateBtn()
        this.getBook();
    }

    updateBtn() {
        this.prevPage = null;
        this.beforeStatus = [BooksStatusName.booksListIdsToRead, BooksStatusName.booksListIdsInProgress, BooksStatusName.booksListIdsDone];
        this.prevPage = this.appService.getStatusBook(this.bookId)
        this.beforeStatus = this.beforeStatus.filter(as => as != this.prevPage);
    }

    changeStatus(bookId: string, prevPage: BooksStatusName, status: BooksStatusName) {
        this.appService.updateStatus(bookId, prevPage, status);
        this.updateBtn();
    }

    getBook() {
        this.book = this.appService.books.find(b => b.id == this.bookId)
    }
}
