import { Component } from '@angular/core';
import {Store} from "@ngrx/store";
import {AppService} from "../../app.service";
import {selectListIdObject, selectTag} from "../../store/selectors";
import {Books, BooksStatusName} from "../../namespace";

@Component({
  selector: 'app-done',
  templateUrl: './done.component.html',
  styleUrls: ['./done.component.less']
})
export class DoneComponent {
    books: Books[];

    subscribeTag;
    subscribeBooks;
    tegForFilter: string = '';

    constructor(public store: Store<any>, appService: AppService) {
        this.subscribeTag = this.store.select(selectTag)
            .subscribe(
                t => {
                   this.tegForFilter = t
                });

        this.subscribeBooks = this.store.select(selectListIdObject)
            .subscribe(
                b => {
                    this.books = appService.books.filter(el => {
                        return b[BooksStatusName.booksListIdsDone][el.id];
                    });
                });
    }

    ngOnDestroy() {
        this.subscribeTag.unsubscribe();
        this.subscribeBooks.unsubscribe();
    }
}
