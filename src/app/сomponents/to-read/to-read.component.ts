import {Component, OnDestroy} from '@angular/core';
import {AppService} from "../../app.service";
import {Books, BooksStatusName} from "../../namespace";
import {selectListIdObject, selectTag} from "../../store/selectors";
import {Store} from "@ngrx/store";

@Component({
    selector: 'app-to-read',
    templateUrl: './to-read.component.html',
    styleUrls: ['./to-read.component.less']
})
export class ToReadComponent implements OnDestroy {
    books: Books[];

    subscribeTag;
    subscribeBooks;
    tegForFilter: string = '';

    constructor(public store: Store<any>, appService: AppService) {
        this.subscribeTag = this.store.select(selectTag)
            .subscribe(
                t => {
                   this.tegForFilter = t
                });

        this.subscribeBooks = this.store.select(selectListIdObject)
            .subscribe(
                b => {
                    this.books = appService.books.filter(el => {
                        return b[BooksStatusName.booksListIdsToRead][el.id];
                    });
                });
    }

    ngOnDestroy() {
        this.subscribeTag.unsubscribe();
        this.subscribeBooks.unsubscribe();
    }

}
