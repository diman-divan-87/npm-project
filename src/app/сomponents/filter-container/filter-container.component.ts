import {Component, OnDestroy} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppService} from "../../app.service";
import {selectTag} from "../../store/selectors";
import {BooksStatusName} from "../../namespace";

import {setFilterTag} from "../../store/actions";

@Component({
    selector: 'app-filter-container',
    templateUrl: './filter-container.component.html',
    styleUrls: ['./filter-container.component.less']
})
export class FilterContainerComponent {
    activeTag: string;

    constructor(public store: Store<any>, public appService: AppService) {
    }

    getTag(tag: string) {
        this.store.dispatch(setFilterTag({tag}))
        this.appService.setQueryParams(tag)
    }


}
