import {Component, OnInit} from '@angular/core';
import {AppService} from "../../app.service";
import {AppServiceFirstStart} from "../../app.service-first-strat";
import {Router, Event as NavigationEvent, NavigationStart} from "@angular/router";
import {take} from "rxjs/operators";

@Component({
    selector: 'app-nav-tabs',
    templateUrl: './nav-tabs.component.html',
    styleUrls: ['./nav-tabs.component.less']
})
export class NavTabsComponent {
    activePage = 'to-read';
    arrActivePage = ['to-read', 'in-progress', 'done'];

    constructor(private router: Router, public appService: AppService, private appServiceFirstStart: AppServiceFirstStart) {
        router.events
            .pipe()
            .subscribe(
                (event: NavigationEvent) => {
                    if (event instanceof NavigationStart) {
                        this.arrActivePage.forEach(ap => {
                            if (event.url.indexOf(ap) == 1) this.activePage = ap;
                        })
                    }
                });

    }

    switchingPage(page: string) {
        this.activePage = page;
        this.appService.switchingPage(page);
    }

}
