import {Component, Input} from '@angular/core';
import {AppService} from "../../app.service";

@Component({
    selector: 'app-book-preview',
    templateUrl: './book-preview.component.html',
    styleUrls: ['./book-preview.component.less']
})
export class BookPreviewComponent {
    @Input() author: string = "";
    @Input() id: string = "";
    @Input() title: string = "";
    @Input() description: string = "";
    @Input() tags: string[] = [];

    constructor(public appService: AppService) {
    }

}
