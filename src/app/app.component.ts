import {Component, OnInit} from '@angular/core';
import {AppService} from "./app.service";
import {BooksStatusName} from "./namespace";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent {

    constructor(private appService: AppService) {
        appService.createObjectsOfLocalStorage()
    }
}
