import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';

import {AppRoutingModule} from './app-routing.module';
import {ToReadComponent} from './сomponents/to-read/to-read.component';
import {InProgressComponent} from './сomponents/in-progress/in-progress.component';
import {DoneComponent} from './сomponents/done/done.component';
import {BookComponent} from './сomponents/book/book.component';
import {FilterContainerComponent} from './сomponents/filter-container/filter-container.component';
import {AppComponent} from './app.component';
import {commonReducers} from "./store/reducers";
import {environment} from "../environments/environment";
import {BookPreviewComponent} from './сomponents/book-preview/book-preview.component';
import {NavTabsComponent} from './сomponents/nav-tabs/nav-tabs.component';
import {BtnChangeStatusComponent} from './simple-components/btn-change-status/btn-change-status.component';
import {TagFilterPipe} from "./pipe";
import { TagComponent } from './simple-components/tag/tag.component';
import { WrapperBookPreviewComponent } from './wrapper-components/wrapper-book-preview/wrapper-book-preview.component';
import { PlaceholderNotResultComponent } from './simple-components/placeholder-not-result/placeholder-not-result.component';
import { WrapperPageComponent } from './wrapper-components/wrapper-page/wrapper-page.component';

@NgModule({
    declarations: [
        ToReadComponent,
        InProgressComponent,
        DoneComponent,
        BookComponent,
        FilterContainerComponent,
        AppComponent,
        BookPreviewComponent,
        NavTabsComponent,
        BtnChangeStatusComponent,
        TagFilterPipe,
        TagComponent,
        WrapperBookPreviewComponent,
        PlaceholderNotResultComponent,
        WrapperPageComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        StoreModule.forRoot({core: commonReducers}),
        StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
