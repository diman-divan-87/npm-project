import {Injectable} from '@angular/core';
import {Books, BooksStatusName, ListIdStatusObject} from "./namespace";
import {Store} from "@ngrx/store";
import {setFilterTag, updateListIdsObject} from './store/actions'
import {filter, take} from "rxjs/operators";
import {ActivatedRoute} from "@angular/router";


@Injectable({
    providedIn: 'root',
})
export class AppServiceFirstStart {

    constructor(private store: Store<any>, private activatedRoute: ActivatedRoute) {}

    checkExistsLocalStorage(books: Books[]) {
        if (localStorage.getItem(BooksStatusName.booksListIdsToRead) === null) {
            const listId = books.reduce((map: any, book) => {
                map[book.id] = true;
                return map;
            }, {});
            localStorage.setItem(BooksStatusName.booksListIdsToRead, JSON.stringify(listId));
        }
        if (localStorage.getItem(BooksStatusName.booksListIdsInProgress) === null) {
            localStorage.setItem(BooksStatusName.booksListIdsInProgress, JSON.stringify({}));
        }
        if (localStorage.getItem(BooksStatusName.booksListIdsDone) === null) {
            localStorage.setItem(BooksStatusName.booksListIdsDone, JSON.stringify({}));
        }
    }

    fillStore() {
        const listIdsObject = this.getListIdsStatusTObject();
        this.store.dispatch(updateListIdsObject({listIdsObject}));

    }

    getListIdsStatusTObject(): ListIdStatusObject {
        return {
            'to-read': JSON.parse(localStorage.getItem(BooksStatusName.booksListIdsToRead) || '{}'),
            'in-progress': JSON.parse(localStorage.getItem(BooksStatusName.booksListIdsInProgress) || '{}'),
            'done': JSON.parse(localStorage.getItem(BooksStatusName.booksListIdsDone) || '{}')
        }
    };

    getTagList(books: Books[]): string[] {
        let list: string[] = [];
        books.forEach(b=> {
            list = [...list, ...b.tags]
        })
        return [...new Set(list)]
    }

    checkQueryParams(): string {
        let tag;
        this.activatedRoute.queryParams
            .pipe(filter(params => params.tag), take(1))
            .subscribe(params => {
                tag = params.tag;
                this.store.dispatch(setFilterTag({tag}))
                return tag;
            })
        return tag;
    }
}
