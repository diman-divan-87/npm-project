export enum BooksStatusName {
    booksListIdsToRead = 'to-read',
    booksListIdsInProgress = 'in-progress',
    booksListIdsDone = 'done'
}

export interface Books {
    "id": string;
    "author": string;
    "title": string;
    "description": string;
    "tags": string[];
}

export type ListIdStatusBook = {
    [key: string]: {
        [key: string]: boolean;
    };
};

export interface ListIdStatusObject {
    'to-read': {[key: string]: boolean};
    'in-progress': {[key: string]: boolean};
    'done': {[key: string]: boolean};

};
