import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WrapperBookPreviewComponent } from './wrapper-book-preview.component';

describe('WrapperBookPreviewComponent', () => {
  let component: WrapperBookPreviewComponent;
  let fixture: ComponentFixture<WrapperBookPreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WrapperBookPreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperBookPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
