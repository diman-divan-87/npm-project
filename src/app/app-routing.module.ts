import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ToReadComponent} from "./сomponents/to-read/to-read.component";
import {BookComponent} from "./сomponents/book/book.component";
import {InProgressComponent} from "./сomponents/in-progress/in-progress.component";
import {DoneComponent} from "./сomponents/done/done.component";


const routes: Routes = [
  {path: '', redirectTo: '/to-read', pathMatch: 'full'},
  {path: 'to-read', component: ToReadComponent},
  {path: 'in-progress', component: InProgressComponent},
  {path: 'done', component: DoneComponent},
  {path: 'book/:id', component: BookComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
