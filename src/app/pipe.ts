import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'tagFilter'
})
export class TagFilterPipe implements PipeTransform {
    transform(items: any[], fields: string[], value: string): any {


        if (!items) {
            return [];
        }
        if (value === undefined || value === null || value === '') {
            return items;
        }
        return items.filter(it => {
            const itsStr = fields.map(f => it[f]) + '';
            return itsStr.toLowerCase().indexOf(value.toLowerCase()) !== -1;
        });
    }
}
