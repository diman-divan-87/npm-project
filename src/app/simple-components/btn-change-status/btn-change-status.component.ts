import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-btn-change-status',
  templateUrl: './btn-change-status.component.html',
  styleUrls: ['./btn-change-status.component.less']
})
export class BtnChangeStatusComponent {
    @Input() name = ''
}
