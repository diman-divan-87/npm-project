import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnChangeStatusComponent } from './btn-change-status.component';

describe('BtnChangeStatusComponent', () => {
  let component: BtnChangeStatusComponent;
  let fixture: ComponentFixture<BtnChangeStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BtnChangeStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnChangeStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
