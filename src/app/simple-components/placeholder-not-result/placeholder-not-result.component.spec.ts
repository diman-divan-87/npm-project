import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceholderNotResultComponent } from './placeholder-not-result.component';

describe('PlaceholderNotResultComponent', () => {
  let component: PlaceholderNotResultComponent;
  let fixture: ComponentFixture<PlaceholderNotResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaceholderNotResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceholderNotResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
