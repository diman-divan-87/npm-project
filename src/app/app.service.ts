import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {take, filter} from "rxjs/operators";
import {Store} from "@ngrx/store";

import booksJson from 'src/books.json'
import {Books, BooksStatusName, ListIdStatusObject} from "./namespace";
import {setFilterTag, updateListIdsObject} from './store/actions'
import {selectListIdObject} from "./store/selectors";
import {AppServiceFirstStart} from "./app.service-first-strat";

@Injectable({
    providedIn: 'root',
})
export class AppService {
    books: Books[];
    tagList: string[];
    activeTag: string;

    constructor(
        public store: Store<any>,
        public appServiceFirstStart: AppServiceFirstStart,
        private router: Router,
        private activatedRoute: ActivatedRoute,) {
    }

    createObjectsOfLocalStorage() {
        this.books = booksJson;
        this.appServiceFirstStart.checkExistsLocalStorage(this.books);
        this.appServiceFirstStart.fillStore()
        this.appServiceFirstStart.checkQueryParams()
        this.tagList = this.appServiceFirstStart.getTagList(this.books);

    }

    updateStatus(idBook: string, prevStatus: BooksStatusName, nextStatus: BooksStatusName) {
        this.deleteIdFromList(idBook)
        this.updateListIdsInStatus(idBook, nextStatus)
    }

    updateListIdsInStatus(idBook: string, statusName: BooksStatusName) {
        this.store.select(selectListIdObject)
            .pipe(take(1))
            .subscribe(
                read => {
                    let listIdsObject = JSON.parse(JSON.stringify(read))
                    for (const listIdsToReadKey in listIdsObject) {
                        if (listIdsToReadKey == statusName) listIdsObject[listIdsToReadKey][idBook] = true;
                    }
                    this.store.dispatch(updateListIdsObject({listIdsObject}));
                    this.updateStatusInLocalStorageObject(listIdsObject);
                });
    }

    deleteIdFromList(idBook: string) {
        this.store.select(selectListIdObject)
            .pipe(take(1))
            .subscribe(
                read => {
                    let listIdsObject = JSON.parse(JSON.stringify(read))
                    for (const listIdsToReadKey in listIdsObject) {
                        if (listIdsObject[listIdsToReadKey][idBook]) delete listIdsObject[listIdsToReadKey][idBook]
                    }
                    this.store.dispatch(updateListIdsObject({listIdsObject}));
                    this.updateStatusInLocalStorageObject(listIdsObject);
                });
    }

    getStatusBook(idBook: string): string {
        let statusKey;
        this.store.select(selectListIdObject)
            .pipe(take(1))
            .subscribe(
                obj => {
                    for (const objKey in obj) {
                        for (const bookIdKey in obj[objKey]) {
                            if (bookIdKey == idBook) statusKey = objKey;
                        }
                    }
                });
        return statusKey;
    }

    updateStatusInLocalStorageObject(listIds: ListIdStatusObject) {
        localStorage.setItem(BooksStatusName.booksListIdsToRead, JSON.stringify(listIds[BooksStatusName.booksListIdsToRead]));
        localStorage.setItem(BooksStatusName.booksListIdsInProgress, JSON.stringify(listIds[BooksStatusName.booksListIdsInProgress]));
        localStorage.setItem(BooksStatusName.booksListIdsDone, JSON.stringify(listIds[BooksStatusName.booksListIdsDone]));
    }

    switchingPage(routerLink: string = '') {
        this.activeTag = this.appServiceFirstStart.checkQueryParams();
        const path = this.router.navigate([routerLink], {
            queryParams: this.activeTag ? {'tag' : this.activeTag} : null,
            relativeTo: this.activatedRoute,
        });
    }

    setQueryParams(tag: string) {
        this.activeTag = tag;
        return this.router.navigate([this.router.url.split('?')[0]],
            {
                queryParams: tag ? {'tag': tag} : null,
                relativeTo: this.activatedRoute,
            })
    }

}
